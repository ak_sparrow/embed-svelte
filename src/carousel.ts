import Carousel from './Carousel.svelte';

const app = new Carousel({
	target: document.body,
});

export default app;